export default class Plateau {
  constructor (xPosition, yPosition) {
    if (!Number.isInteger(xPosition) || !Number.isInteger(yPosition)) {
      throw new Error('Plateau Positions should be integers');
    }

    if (xPosition <= 0 || yPosition <= 0) {
      throw new Error('Plateau Positions should be positive');
    }

    this.xPosition = xPosition;
    this.yPosition = yPosition;
  }
}