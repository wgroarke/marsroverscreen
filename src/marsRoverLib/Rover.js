const ORIENTATIONS = ['W', 'N', 'E', 'S'];

export default class Rover {
  constructor (xPosition, yPosition, orientation) {
    if (!Number.isInteger(xPosition) || !Number.isInteger(yPosition)) {
      throw new Error('Rover Positions should be integers');
    }

    if (xPosition < 0 || yPosition < 0) {
      throw new Error('Rover Positions should be non-negative');
    }

    if (ORIENTATIONS.indexOf(orientation) < 0) {
      throw new Error('Rover orientation should be one of "W, N, E, S"');
    }

    this.xPosition = xPosition;
    this.yPosition = yPosition;
    this.orientation = orientation;
  }

  move () {
    switch (this.orientation) {
      case 'W': {
        this.xPosition--;
        break;
      }
      case 'N': {
        this.yPosition++;
        break;
      }
      case 'E': {
        this.xPosition++;
        break;
      }
      case 'S': {
        this.yPosition--;
        break;
      }
      default:
        break;
    }
  }

  turnLeft () {
    const orientationIndex = ORIENTATIONS.indexOf(this.orientation) - 1;
    this.orientation = ORIENTATIONS[orientationIndex < 0 ? orientationIndex + 4 : orientationIndex];
  }

  turnRight () {
    const orientationIndex = ORIENTATIONS.indexOf(this.orientation) + 1;
    this.orientation = ORIENTATIONS[orientationIndex > 3 ? orientationIndex - 4 : orientationIndex];
  }
}
