import React, {
  Component,
  PropTypes,
} from 'react';
import robotSvg from '../../assets/robot.svg';

import cx from 'classname';
import style from './Rover.scss';

import {WIDTH, HEIGHT} from '../Grid/Grid';

const ROBOT_WIDTH = 30;
const ROBOT_HEIGHT = 30;

class Rover extends Component {
  render () {
    return (
      <div className={cx(style.rover, style[this.props.rover.orientation])}
           style={{
             left: WIDTH * (this.props.rover.xPosition / this.props.plateauXPosition) - ROBOT_WIDTH / 2,
             bottom: HEIGHT * (this.props.rover.yPosition / this.props.plateauYPosition) - ROBOT_HEIGHT / 2,
           }}
      >
        <img src={robotSvg}
             width={ROBOT_WIDTH}
             height={ROBOT_HEIGHT}
        />
      </div>
    );
  }
}

Rover.propTypes = {
  rover: PropTypes.shape({
    xPosition: PropTypes.number.isRequired,
    yPosition: PropTypes.number.isRequired,
    orientation: PropTypes.string.isRequired,
  }).isRequired,
  plateauXPosition: PropTypes.number.isRequired,
  plateauYPosition: PropTypes.number.isRequired,
};
Rover.defaultProps = {
  plateauXPosition: 1,
  plateauYPosition: 1,
};

export default Rover;
